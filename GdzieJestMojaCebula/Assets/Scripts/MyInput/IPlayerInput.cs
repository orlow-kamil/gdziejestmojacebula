﻿using System.Collections;
using UnityEngine;

namespace MyInput
{
    public enum WhichWeapon
    {
        First, 
        Second, 
        Third
    }

    public abstract class IPlayerInput : ScriptableObject
    {
        public bool IsMove => Mathf.Abs(Vertical) + Mathf.Abs(Horizontal) != 0f;

        public virtual float Vertical { get; }
        public virtual float Horizontal { get; }


        public virtual Vector2 PointerPos { get; }
        public Vector2 PreviousPointerPos { get; private set; }

        public IEnumerator CalculatePreviousPointerPos()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();
                PreviousPointerPos = PointerPos;
            }
        }

        public bool IsAnyAction => Weapon || ChangeHand || IsPointerMove || Attack;

        public bool Weapon => FirstWeapon || SecondWeapon || ThirdWeapon;
        public virtual bool FirstWeapon { get; }
        public virtual bool SecondWeapon { get; }
        public virtual bool ThirdWeapon { get; }


        public int ChooseWhichWeaponUse()
        {
            if (FirstWeapon) return (int)WhichWeapon.First;
            if (SecondWeapon) return (int)WhichWeapon.Second;
            if (ThirdWeapon) return (int)WhichWeapon.Third;
            return default;
        }


        public virtual bool ChangeHand { get; }
        public virtual bool IsPointerMove { get; }
        public virtual bool Attack { get; }
        public virtual bool Interaction { get; }


    }
}