﻿using UnityEngine;

namespace MyInput
{
    [CreateAssetMenu(fileName = "Keyboard", menuName = "ScriptableObjects/PlayerInput/KeyboardInput", order = 1)]
    public class KeyboardInput : IPlayerInput
    {
        [SerializeField] private KeyCode firstWeaponKeyCode = KeyCode.Alpha1;
        [SerializeField] private KeyCode secondWeaponKeyCode = KeyCode.Alpha2;
        [SerializeField] private KeyCode thirdWeaponKeyCode = KeyCode.Alpha3;
        [SerializeField] private KeyCode attackKeyCode = KeyCode.Space;
        [SerializeField] private KeyCode changeHandKeyCode = KeyCode.Tab;
        [SerializeField] private KeyCode interactionKeyCode = KeyCode.F;

        public override float Vertical => Input.GetAxis("Vertical");
        public override float Horizontal => Input.GetAxis("Horizontal");
        public override Vector2 PointerPos => Input.mousePosition;


        public override bool FirstWeapon => Input.GetKeyDown(firstWeaponKeyCode);
        public override bool SecondWeapon => Input.GetKeyDown(secondWeaponKeyCode);
        public override bool ThirdWeapon => Input.GetKeyDown(thirdWeaponKeyCode);
        public override bool ChangeHand => Input.GetKeyDown(changeHandKeyCode);
        public override bool IsPointerMove => (PointerPos - PreviousPointerPos).magnitude > 0.01f;

        public override bool Attack => Input.GetKeyDown(attackKeyCode);
        public override bool Interaction => Input.GetKeyDown(interactionKeyCode);
    }
}