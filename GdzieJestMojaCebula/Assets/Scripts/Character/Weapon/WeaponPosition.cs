﻿using Objects.Weapons;
using UnityEngine;

namespace Character.Weapon
{
    public enum TypeHand
    {
        Right,
        Left
    }

    public class WeaponPosition : MonoBehaviour
    {
        public static int RighHandIndex = 0;
        public static Transform PlayerTransform => FindObjectOfType<ICharacterController>().transform;

        [SerializeField] private TypeHand hand = TypeHand.Right;

        public int Index => (int)hand;
        public Transform Transform => this.transform;
        public IWeapon Weapon
        {
            get
            {
                if (this.transform.childCount == 0) return null;
                GameObject child = this.transform.GetChild(0).gameObject;
                if (child.TryGetComponent<IWeapon>(out IWeapon weapon))
                    return weapon;
                return null;
            }
        }
    }
}