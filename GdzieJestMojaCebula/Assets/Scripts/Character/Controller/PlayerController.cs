﻿using Data.Character;
using UnityEngine;
using Character.Mechanic;
using MyInput;
using Objects;

namespace Character
{
    [RequireComponent(typeof(Collider2D))]
    public class PlayerController : ICharacterController
    {
        protected event System.Action playerEvent;

        protected ICharacterData PlayerData => (PlayerData)characterData;
        public IPlayerInput PlayerInput => playerInput;

        protected override void OnEnable()
        {
            base.OnEnable();
            this.SetupCharacterOption(playerInput);
        }

        private void SetupCharacterOption(IPlayerInput input)
        {
            AddWeaponSystemToPlayerEvent();
            StartCoroutine(input.CalculatePreviousPointerPos());
        }

        private void AddWeaponSystemToPlayerEvent()
        {
            playerEvent += PullOrHideWeapon;
            playerEvent += ChangeWeapon;
            playerEvent += ChangeHand;
            playerEvent += Aim;
            playerEvent += Attack;
        }

        protected override void PullOrHideWeapon() => characterMechanic.PullOrHideWeapon(playerInput, playerAnimator);
        protected override void ChangeWeapon() { }
        protected override void ChangeHand() => characterMechanic.ChangeHandForWeapon(playerInput, playerAnimator);
        protected override void Aim() => characterMechanic.Aim(playerInput);
        protected override void Attack() => characterMechanic.Attack(playerInput, playerAnimator);


        private void Update()
        {
            if (playerInput.IsMove)
            {
                this.transform.position += characterMechanic.Move(playerInput, characterData.Speed, playerAnimator);
                SetAndPlayMovementAudioClip(this.PlayerData);
            }

            if (playerInput.IsAnyAction)
                playerEvent?.Invoke();

            characterMechanic.DelayTimeForNextAttack += Time.deltaTime;
            timeForMusic -= Time.deltaTime;
        }

        private void SetAndPlayMovementAudioClip(ICharacterData playerData)
        {
            if (timeForMusic <= 0f)
            {
                AudioClip rndClip = playerData.GetRandomMovementAudioClip();
                playerAudioSource?.PlayOneShot(rndClip);
                timeForMusic = rndClip.length;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.TryGetComponent(out Collectable pickupable))
            {
                characterEquipment.AddToEquipment(pickupable);
            }
        }
    }
}
