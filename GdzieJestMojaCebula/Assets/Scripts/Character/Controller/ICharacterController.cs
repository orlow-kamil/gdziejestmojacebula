﻿using Data.Character;
using Character.Body;
using UnityEngine;
using Character.Stats;
using Character.Mechanic;
using MyInput;
using Character.Equipment;

namespace Character
{
    [RequireComponent(typeof(AudioSource), typeof(Animator))]
    [RequireComponent(typeof(IHealth), typeof(CharacterEquipment))]
    public abstract class ICharacterController : MonoBehaviour
    {
        [SerializeField] protected IPlayerInput playerInput = null;
        [SerializeField] protected ICharacterData characterData = null;
        [SerializeField] protected ICharacterMechanic characterMechanic;

        protected float timeForMusic = 0f;
        protected AudioSource playerAudioSource;
        protected Animator playerAnimator;
        protected IHealth playerHealth;
        protected CharacterEquipment characterEquipment;

        public CharacterSprite PlayerSprite { get; private set; }

        protected void Awake()
        {
            playerAudioSource = GetComponent<AudioSource>();
            playerAnimator = GetComponent<Animator>();
            playerHealth = GetComponent<IHealth>();
            characterEquipment = GetComponent<CharacterEquipment>();
            this.PlayerSprite = FindObjectOfType<CharacterSprite>();
        }

        protected virtual void OnEnable()
        {
            if (characterData is null)
                throw new System.NullReferenceException("Player data is missing");

            if (playerInput is null)
                throw new System.NullReferenceException("Player input is missing");

            SetupCharacterOption(characterData, characterMechanic);
        }

        private void SetupCharacterOption(ICharacterData data, ICharacterMechanic characterMechanic)
        {
            playerHealth.SetCharacterStats(data);
            characterEquipment.SetStartEquipmentList(data);
            this.PlayerSprite.SetPlayerSprite(data.StartSprite);
            characterMechanic.SetupPlayerMechanic(data);
        }

        protected abstract void PullOrHideWeapon();
        protected abstract void ChangeWeapon();
        protected abstract void ChangeHand();
        protected abstract void Aim();
        protected abstract void Attack();
    }
}