﻿using UnityEngine;

namespace Character.Stats
{
    public class EnemyHealth : IHealth
    {
        public override void Death()
        {
            Instantiate(deadPlayerPrefab, transform.position, Quaternion.identity);
            // extra effects : light + particles
            Debug.Log($"{gameObject.name}: death");
            Destroy(gameObject);
        }
    }
}
