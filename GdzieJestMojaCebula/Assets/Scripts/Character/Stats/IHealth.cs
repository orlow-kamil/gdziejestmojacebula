﻿using Data.Character;
using UnityEngine;

namespace Character.Stats
{
    public abstract class IHealth : MonoBehaviour
    {
        protected GameObject deadPlayerPrefab;
        protected int CurrentHP { get; set; }

        public void SetCharacterStats(ICharacterData characterData)
        {
            SetCurrentHP(characterData);
            deadPlayerPrefab = characterData.DeathCharacter;
        }

        private void SetCurrentHP(ICharacterData characterData) => CurrentHP = characterData.MaxHP;

        public void GetDamage(int damage)
        {
            CurrentHP -= damage;
            if (CurrentHP <= 0)
                Death();
        }
        public abstract void Death();
    }
}
