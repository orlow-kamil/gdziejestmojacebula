﻿using UnityEngine;

namespace Character.Body
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class CharacterSprite : MonoBehaviour
    {
        public SpriteRenderer SpriteRenderer { get; private set; }

        private void Awake() => this.SpriteRenderer = GetComponent<SpriteRenderer>();

        public void SetPlayerSprite(Sprite sprite) => this.SpriteRenderer.sprite = sprite;
    }
}