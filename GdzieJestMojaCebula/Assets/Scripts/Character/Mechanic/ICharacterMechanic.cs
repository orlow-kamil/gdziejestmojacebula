﻿using Data.Character;
using Character.Weapon;
using UnityEngine;
using Character.Mechanic.Skill;
using MyInput;
using Objects.Weapons;

namespace Character.Mechanic
{
    [System.Serializable]
    public class ICharacterMechanic
    {
        [SerializeField] private MovementSystem movementSystem;
        [SerializeField] private WeaponSystem weaponSystem;

        public float DelayTimeForNextAttack { get => _delayTimeForNextAttack; set => _delayTimeForNextAttack = value; }

        protected int CurrentUsedEquipmentSlotNumber { get; private set; }
        protected WeaponPosition CurrentWeaponPosition => weaponSystem.CurrentWeaponPosition;
        protected IWeapon CurrentWeapon => weaponSystem.CurrentWeapon;

        private float _delayTimeForNextAttack;

        public Vector3 Move(IPlayerInput input, float speed, Animator animator)
        {
            movementSystem.SetXYIntoAnimation(animator);
            return movementSystem.Move(input, speed);
        }

        public void SetupPlayerMechanic(ICharacterData characterData)
        {
            if (characterData is null)
                throw new System.NullReferenceException($"PlayerData is missing");

            weaponSystem.PlayerData = (PlayerData)characterData;
            weaponSystem.FindAndSetupEveryWeaponPosToList();
            weaponSystem.RandomSetCurrentWeaponPosition();
        }


        public void PullOrHideWeapon(IPlayerInput input, Animator animator)
        {
            if (!weaponSystem.IsWeaponActive)
                PullWeapon(input, animator);
            else
            {
                HideWeapon(input);
                if (!(this.CurrentUsedEquipmentSlotNumber == input.ChooseWhichWeaponUse()))
                    PullWeapon(input, animator);
            }
        }

        private void PullWeapon(IPlayerInput input, Animator animator)
        {
            if (input.Weapon)
            {
                weaponSystem.RandomSetCurrentWeaponPosition();
                this.CurrentUsedEquipmentSlotNumber = input.ChooseWhichWeaponUse();
                weaponSystem.PullWeapon(weaponSystem.StartWeaponPosition, this.CurrentUsedEquipmentSlotNumber, animator);
            }
        }

        private void HideWeapon(IPlayerInput input)
        {
            if (input.Weapon)
                weaponSystem.HideWeapon(CurrentWeaponPosition);
        }

        public void ChangeHandForWeapon(IPlayerInput input, Animator animator)
        {
            if (input.ChangeHand)
            {
                weaponSystem.ChangeHandForWeapon(CurrentUsedEquipmentSlotNumber, animator);
            }
        }

        public void Aim(IPlayerInput input) => CurrentWeapon?.Aim(input, CurrentWeaponPosition);

        public void Attack(IPlayerInput input, Animator animator) => CurrentWeapon?.Attack(input, animator, ref _delayTimeForNextAttack);

    }
}
