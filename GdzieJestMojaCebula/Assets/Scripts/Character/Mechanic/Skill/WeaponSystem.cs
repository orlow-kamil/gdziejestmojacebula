﻿using Data.Character;
using Character.Weapon;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Objects.Weapons;

namespace Character.Mechanic.Skill
{
    [System.Serializable]
    public class WeaponSystem
    {
        public PlayerData PlayerData { get; set; }

        public List<WeaponPosition> WeaponPositionsList { get; private set; }
        public WeaponPosition StartWeaponPosition { get; private set; }
        public WeaponPosition CurrentWeaponPosition { get; private set; }
        public IWeapon CurrentWeapon => IsWeaponActive ? _currentHoldWeapon : null;

        public bool IsWeaponActive => _currentHoldWeapon is object;
        public bool IsWeaponInRightHand => CurrentWeaponPosition.Index == WeaponPosition.RighHandIndex;

        private IWeapon _currentHoldWeapon;


        public void FindAndSetupEveryWeaponPosToList()
        {
            WeaponPositionsList = Object.FindObjectsOfType<WeaponPosition>().ToList();
            if (WeaponPositionsList[0].Index != WeaponPosition.RighHandIndex)
                WeaponPositionsList.Reverse();
        }

        public void RandomSetCurrentWeaponPosition() => StartWeaponPosition = WeaponPositionsList[Random.Range(0, WeaponPositionsList.Count)];

        public void PullWeapon(WeaponPosition weaponPosition, int slotNumber, Animator animator)
        {
            if (!IsWeaponActive && slotNumber < PlayerData.StartEquipmentWeaponsList.Count)
            {
                _currentHoldWeapon = PlayerData.GetWeaponFromList(slotNumber);
                CurrentWeaponPosition = weaponPosition;
                SetCurrentEquipmentWeaponSlotNumberToWeapon(slotNumber);
                animator.SetBool("IsRightHand", IsWeaponInRightHand);
                SetWeaponToPlayerHand(weaponPosition, _currentHoldWeapon);
            }
        }

        private void SetCurrentEquipmentWeaponSlotNumberToWeapon(int slotNumber) => CurrentWeapon.CurrentEquipmentWeaponSlotNumber = slotNumber;

        private void SetWeaponToPlayerHand(WeaponPosition weaponPosition, IWeapon weapon)
        {
            GameObject currentWeapon = Object.Instantiate(weapon.gameObject, weaponPosition.Transform.position, weaponPosition.Transform.rotation);
            currentWeapon.transform.SetParent(weaponPosition.Transform);

            _currentHoldWeapon = currentWeapon.GetComponent<IWeapon>();
            _currentHoldWeapon.SetupWeapon();
            _currentHoldWeapon.SetAndPlayAudioClipToAudioSource();
        }

        private WeaponPosition GetWeaponPositionFromIndex(int index) => WeaponPositionsList[index];

        public void HideWeapon(WeaponPosition weaponPosition)
        {
            if (IsWeaponActive)
                RemoveWeaponToPlayerHand(weaponPosition);
        }

        private void RemoveWeaponToPlayerHand(WeaponPosition weaponPosition)
        {
            IWeapon holdWeapon = weaponPosition.Weapon;
            Object.Destroy(holdWeapon.gameObject);
            CurrentWeaponPosition = null;
            _currentHoldWeapon = null;
        }

        public void ChangeHandForWeapon(int slotNumber, Animator animator)
        {
            if (IsWeaponActive)
            {
                int currentIndex = CurrentWeaponPosition.Index;
                int newIndex = currentIndex + 1 < WeaponPositionsList.Count ? currentIndex + 1 : 0;
                ChangeHandForIndexes(slotNumber, currentIndex, newIndex, animator);
            }
        }

        private void ChangeHandForIndexes(int slotNumber, int currentIndex, int newIndex, Animator animator)
        {
            HideWeapon(GetWeaponPositionFromIndex(currentIndex));
            PullWeapon(GetWeaponPositionFromIndex(newIndex), slotNumber, animator);
        }
    }
}