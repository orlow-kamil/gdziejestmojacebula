﻿using UnityEngine;
using MyInput;

namespace Character.Mechanic.Skill
{
    [System.Serializable]
    public class MovementSystem
    {
        private float h;
        private float v;

        public Vector3 Move(IPlayerInput input, float speed)
        {
            h = input.Horizontal;
            v = input.Vertical;

            Vector3 move = new Vector3(h, v, 0f);
            return move * speed * Time.deltaTime;
        }

        public void SetXYIntoAnimation(Animator animator)
        {
            animator.SetFloat("x", h);
            animator.SetFloat("y", v);
        }

    }
}