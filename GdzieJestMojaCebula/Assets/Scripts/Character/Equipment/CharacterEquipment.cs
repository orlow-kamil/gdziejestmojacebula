﻿using Data.Character;
using Objects;
using System.Collections.Generic;
using UI.Gameplay;
using UnityEngine;

namespace Character.Equipment
{
    public class CharacterEquipment : MonoBehaviour
    {
        protected List<IItem> equipmentPickupableList = new List<IItem>();

        protected UIEquipment uIEquipment;

        private void OnEnable()
        {
            uIEquipment = FindObjectOfType<UIEquipment>();
            if (uIEquipment is null)
                throw new System.NullReferenceException($"UI Equipment is missing");
        }

        public void SetStartEquipmentList(ICharacterData characterData)
        {
            int i = 0;
            foreach (var weapon in characterData.StartEquipmentWeaponsList)
            {
                weapon.SetupItem();
                this.equipmentPickupableList.Add(weapon.Item);
                uIEquipment.SetupWeaponToEquipmentSlot(weapon.Item.ObjectSprite, i++);
            }
        }


        public void AddToEquipment(Collectable pickupable)
        {
            pickupable.SetupItem();
            this.equipmentPickupableList.Add(pickupable.Item);
            int freeIndex = uIEquipment.GetAvaviableIndexFromEquipmentSlotList();
            uIEquipment.SetupWeaponToEquipmentSlot(pickupable.Item.ObjectSprite, freeIndex);
            DestroyImmediate(pickupable.Item.GameObject, true);
        }

        public void RemoveFromEquipment(Collectable pickupable)
        {
            this.equipmentPickupableList.Remove(pickupable.Item);
            int index = uIEquipment.GetIndexFromEquipmentSlotList(pickupable.Item.ObjectSprite);
            uIEquipment.RemoveWeaponFromEquipmentSlot(index);
            pickupable = null;
        }
    }
}