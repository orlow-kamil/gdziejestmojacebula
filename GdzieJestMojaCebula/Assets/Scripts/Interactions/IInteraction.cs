﻿using Character.Equipment;

namespace Interactions
{
    public interface IInteraction
    {
        void Trigger(CharacterEquipment characterEquipment);
    }
}