﻿using Character;
using Character.Equipment;
using Objects;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Interactions
{
    [RequireComponent(typeof(Collider2D))]
    public class EquipmentInteraction : MonoBehaviour, IInteraction
    {
        [SerializeField] private TextMeshProUGUI interactionTextMesh = null;

        [SerializeField] private string enterText = "";
        [SerializeField] private string stayText = "";
        [SerializeField] private string doneText = "";

        [SerializeField] private Collectable interactionCollectable = null;

        private CharacterEquipment characterEquipment;
        private bool onlyOnce = true;

        public void Trigger(CharacterEquipment characterEquipment)
        {
            if (onlyOnce)
            {
                onlyOnce = false;
                characterEquipment.AddToEquipment(interactionCollectable);
                Destroy(this.gameObject, .5f);
                interactionTextMesh.enabled = false;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.TryGetComponent(out characterEquipment))
            {
                StartCoroutine(DisplayAndChangeText());
            }
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.TryGetComponent(out PlayerController player))
            {
                if (player.PlayerInput.Interaction)
                {
                    interactionTextMesh.text = doneText;
                    Trigger(characterEquipment);
                }
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.TryGetComponent(out CharacterEquipment characterEquipment))
            {
                interactionTextMesh.enabled = false;
            }
        }

        private IEnumerator DisplayAndChangeText()
        {
            interactionTextMesh.enabled = true;
            interactionTextMesh.text = enterText;
            yield return new WaitForSeconds(1.0f);
            interactionTextMesh.text = stayText;
        }
    }
}