﻿using Objects.Weapons;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Data.Character
{
    public abstract class ICharacterData : ScriptableObject
    {
        [Header("Stats")]
        [SerializeField] private List<IWeapon> startEquipmentWeaponsList = new List<IWeapon>();
        [SerializeField] [Range(50, 500)] private int maxHP = 100;
        [SerializeField] [Range(0, 10)] private float speed = 5f;

        [Header("Arts")]
        [SerializeField] private List<AudioClip> movementAudioClipsList = new List<AudioClip>();
        [SerializeField] private Sprite startSprite = null;
        [SerializeField] private GameObject deathCharacter = null;

        public List<IWeapon> StartEquipmentWeaponsList => startEquipmentWeaponsList;
        public float Speed { get => speed; set => speed = value; }
        public int MaxHP => maxHP;
        protected List<AudioClip> MovementAudioClipsList => movementAudioClipsList;
        public Sprite StartSprite => startSprite;
        public GameObject DeathCharacter => deathCharacter;

        public AudioClip GetRandomMovementAudioClip() => this.MovementAudioClipsList.ElementAt(new System.Random().Next(0, this.MovementAudioClipsList.Count));
    }
}
