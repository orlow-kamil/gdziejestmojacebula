﻿using UnityEngine;
using Objects.Weapons;

namespace Data.Character
{
    [CreateAssetMenu(fileName = "Player", menuName = "ScriptableObjects/Data/PlayerData", order = 1)]
    public class PlayerData : ICharacterData
    {
        public IWeapon GetWeaponFromList(int slotNumber)
        {
            if (slotNumber < this.StartEquipmentWeaponsList.Count)
                return this.StartEquipmentWeaponsList[slotNumber];
            return default;
        }
    }
}