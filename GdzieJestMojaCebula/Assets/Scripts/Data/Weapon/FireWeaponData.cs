﻿using Objects.Weapons;
using UnityEngine;

namespace Data.Weapon
{
    [CreateAssetMenu(fileName = "FireWeapon", menuName = "ScriptableObjects/Data/Weapon/FireWeapon", order = 2)]
    public class FireWeaponData : IWeaponData
    {
        private const int MinPercentAmmo = 10;

        [SerializeField] private Bullet bulletPrefab = null;
        [SerializeField] private int maxAmmountAmmo = 50;
        [SerializeField] [Range(0, 100f)] private int fireForce = 25;

        public override Bullet BulletPrefab => bulletPrefab;
        public override Transform GunMuzzle => FindObjectOfType<Muzzle>().Transform;

        public int CurrentAmmo { get; private set; }
        public override bool OutOfAmmo => this.CurrentAmmo == 0;

        public override int PushForceMultiplier => fireForce * Multiplier;

        public override void SetStartAmmo() => this.CurrentAmmo = Random.Range(this.MinAmountAmmo, maxAmmountAmmo);
        public int MinAmountAmmo => MinPercentAmmo / 100 * maxAmmountAmmo;
    }
}
