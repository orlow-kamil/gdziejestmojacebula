﻿using UnityEngine;

namespace Data.Weapon
{
    [CreateAssetMenu(fileName = "MeleeWeapon", menuName = "ScriptableObjects/Data/Weapon/MeleeWeapon", order = 1)]
    public class MeleeWeaponData : IWeaponData
    {
        [SerializeField] [Range(0, 100)] private int pushForce = 5;
        [SerializeField] [Range(0, 100)] private float durability = 100f;

        public override int PushForceMultiplier => pushForce * Multiplier;
        public float Durability { get => durability; set => durability = value; }
    }
}
