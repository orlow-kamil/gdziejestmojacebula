﻿using Objects.Weapons;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Data.Weapon
{
    public abstract class IWeaponData : ScriptableObject
    {
        protected const int Multiplier = 10;

        [SerializeField] [Range(0.5f, 2.5f)] private float reloadTime = 1.5f;
        [SerializeField] [Range(0, 45)] int halfAngleRotatation = 60;

        // Player = 100HP
        [SerializeField] [Range(1, 50)] private int minDmg = 20;
        [SerializeField] [Range(50, 100)] private int maxDmg = 50;


        [Space]
        [Header("Arts")]
        [SerializeField] private List<AudioClip> attackAudioClipsList = new List<AudioClip>();
        [SerializeField] private List<AudioClip> movementAudioClipsList = new List<AudioClip>();

        public int Damage { get; set; }
        public float ReloadTime => reloadTime;
        public float HalfAngleRotation => halfAngleRotatation;
        public int MinDmg => minDmg;
        public int MaxDmg => maxDmg;


        public AudioClip GetRandomAttackAudioClip() => attackAudioClipsList.ElementAtOrDefault(new System.Random().Next(0, attackAudioClipsList.Count));
        public AudioClip GetRandomMovementAudioClip() => movementAudioClipsList.ElementAtOrDefault(new System.Random().Next(0, movementAudioClipsList.Count));



        public virtual Bullet BulletPrefab { get; }
        public virtual Transform GunMuzzle { get; }
        public virtual bool OutOfAmmo { get; }
        public virtual int PushForceMultiplier { get; }
        public virtual void SetStartAmmo() { }
    }
}