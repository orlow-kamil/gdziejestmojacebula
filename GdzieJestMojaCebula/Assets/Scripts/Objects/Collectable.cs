﻿using Data.Weapon;
using UnityEngine;

namespace Objects
{
    public class IItem
    {
        public GameObject GameObject { get; set; }
        public Sprite ObjectSprite { get; set; }
    }

    public class WeaponItem : IItem
    {
        public IWeaponData WeaponData { get; set; }
    }

    public class Collectable : MonoBehaviour
    {
        [SerializeReference] public IItem Item;

        protected virtual void OnEnable()
        {
            SetupItem();
        }

        public virtual void SetupItem()
        {
            Item = new WeaponItem
            {
                GameObject = this.gameObject,
                ObjectSprite = GetComponentInChildren<SpriteRenderer>().sprite,
            };
        }
    }
}