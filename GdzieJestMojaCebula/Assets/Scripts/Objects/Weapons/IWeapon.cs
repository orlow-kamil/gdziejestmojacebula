﻿using UnityEngine;
using Character.Weapon;
using Data.Weapon;
using System.Collections;
using MyInput;

namespace Objects.Weapons
{
    [RequireComponent(typeof(AudioSource))]
    public abstract class IWeapon : Collectable
    {
        private const int StraightAngleInDegree = 90;
        private const int SpeedRotationReaction = 10;

        [SerializeField] protected IWeaponData weaponData = null;
        protected AudioSource weaponAudioSource;
        public IWeaponData WeaponData => weaponData;
        public int CurrentEquipmentWeaponSlotNumber { get; set; }

        [SerializeReference] public WeaponItem WeaponItem;

        Vector3 diffrence;
        float rotationZ;
        float rotZ;

        private void Awake()
        {
            weaponAudioSource = GetComponent<AudioSource>();
        }

        protected override void OnEnable()
        {
            if (weaponData is null)
                throw new System.NullReferenceException("Weapon data is missing");

            SetupItem();
        }

        public override void SetupItem()
        {
            Item = new WeaponItem
            {
                GameObject = this.gameObject,
                ObjectSprite = GetComponentInChildren<SpriteRenderer>().sprite,
                WeaponData = weaponData,
            };
        }

        public void SetAndPlayAudioClipToAudioSource()
        {
            AudioClip clip = WeaponData.GetRandomMovementAudioClip();
            weaponAudioSource?.PlayOneShot(clip);
        }

        public void Aim(IPlayerInput input, WeaponPosition weaponPosition)
        {
            if (weaponPosition is null) return;
            Vector3 mousePosInWorld = Camera.main.ScreenToWorldPoint(input.PointerPos);
            diffrence = (mousePosInWorld - WeaponPosition.PlayerTransform.position).normalized;

            float dot = Vector3.Dot(WeaponPosition.PlayerTransform.right, diffrence);
            int sign = weaponPosition.Index == WeaponPosition.RighHandIndex ? 1 : -1;

            if (dot * sign <= 0f) return;

            rotationZ = Mathf.Atan2(diffrence.x, diffrence.y) * Mathf.Rad2Deg;

            rotZ = Mathf.Clamp(rotationZ, sign * StraightAngleInDegree - weaponData.HalfAngleRotation, sign * StraightAngleInDegree + weaponData.HalfAngleRotation);
            Quaternion endRotation = Quaternion.Euler(0f, 0f, sign * rotZ) * weaponPosition.Transform.rotation;

            transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, SpeedRotationReaction * Time.deltaTime);
        }

        public int CalculateDamage() => Random.Range(weaponData.MinDmg, weaponData.MaxDmg);
        public void Destroy() => Destroy(gameObject);

        public virtual void SetupWeapon() => Debug.Log($"Setup {name}");
        public abstract void Attack(IPlayerInput input, Animator animator, ref float time);

        protected IEnumerator AnimationAttack(Animator animator)
        {
            animator.SetTrigger("Attacking");

            AnimatorStateInfo animationState = animator.GetCurrentAnimatorStateInfo(0);
            AnimatorClipInfo[] myAnimatorClip = animator.GetCurrentAnimatorClipInfo(0);
            float time = myAnimatorClip[0].clip.length * animationState.normalizedTime;

            yield return new WaitForSeconds(time);
            animator.ResetTrigger("Attacking");
        }
    }
}