﻿using Data.Weapon;
using UnityEngine;
using MyInput;

namespace Objects.Weapons
{
    public class FireWeapon : IWeapon
    {
        public IWeaponData WeapondData => (FireWeaponData)WeaponData;

        public override void SetupWeapon()
        {
            if (WeapondData.GunMuzzle is null)
                throw new System.Exception($"{gameObject.name}: muzzle is missing");

            if (WeapondData.BulletPrefab is null)
                throw new System.Exception($"{gameObject.name}: bullet prefab is missing");

            WeapondData.SetStartAmmo();
        }



        public override void Attack(IPlayerInput input, Animator animator, ref float time)
        {
            if (input.Attack && time >= weaponData.ReloadTime && !weaponData.OutOfAmmo)
            {
                StartCoroutine(AnimationAttack(animator));
                Fire();
                AudioClip clip = weaponData.GetRandomAttackAudioClip();
                weaponAudioSource?.PlayOneShot(clip);
                Debug.Log($"Attack from {gameObject.name}");
                time = 0;
            }
        }

        private void Fire()
        {
            Transform gunMuzzle = WeapondData.GunMuzzle;
            GameObject bullet = Instantiate(WeapondData.BulletPrefab.gameObject, gunMuzzle.position, gunMuzzle.rotation);
            bullet.GetComponent<Bullet>().BulletDirection = transform.up * WeapondData.PushForceMultiplier;
        }
    }
}
