﻿using UnityEngine;

namespace Objects.Weapons
{
    [RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
    public class Bullet : MonoBehaviour
    {
        [SerializeField] [Range(1.5f, 5f)] private float lifeTime = 5f;
        public Vector2 BulletDirection { get; set; }
        private Rigidbody2D _rgb2D;

        private void OnEnable() => _rgb2D = GetComponent<Rigidbody2D>();
        private void Start() => _rgb2D.AddForce(BulletDirection);

        private void OnTriggerEnter2D(Collider2D collision)
        {
            DestroyBullet();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            DestroyBullet();
        }

        private void DestroyBullet()
        {
            // explosion
            // damage
            Destroy(gameObject, lifeTime);
        }
    }
}