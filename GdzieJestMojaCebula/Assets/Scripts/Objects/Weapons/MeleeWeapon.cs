﻿using Data.Weapon;
using UnityEngine;
using MyInput;
using Objects.Weapons;
using Character.Stats;

namespace Objects.Weapon
{
    public class MeleeWeapon : IWeapon
    {
        public IWeaponData WeapondData => (MeleeWeaponData)WeaponData;

        public override void Attack(IPlayerInput input, Animator animator, ref float time)
        {
            if (input.Attack && time >= WeapondData.ReloadTime)
            {
                StartCoroutine(AnimationAttack(animator));
                AudioClip clip = weaponData.GetRandomAttackAudioClip();
                weaponAudioSource?.PlayOneShot(clip);
                Debug.Log($"Attack from {gameObject.name}");
                time = 0f;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.TryGetComponent(out EnemyHealth enemy))
            {
                int dmg = CalculateDamage();
                enemy.GetDamage(dmg);
                Debug.Log($"Attack: deal {dmg}");
            }
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.TryGetComponent(out Rigidbody2D enemyRgb))
            {
                //Vector2 direction = (collision.transform.position - this.transform.position).normalized;
                //enemyRgb.AddForce(direction * this.WeapondData.PushForceMultiplier);
            }
        }
    }
}