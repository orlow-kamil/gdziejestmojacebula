﻿using UnityEngine;

namespace Intro
{
    [RequireComponent(typeof(Collider2D))]
    public class RoomZone : MonoBehaviour
    {
        [SerializeField] private IntroManager introManager = null;
        [SerializeField] private bool isMyRoom;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            introManager.ChangeRoom(!isMyRoom);
            introManager.IntroAudioSource.Stop();
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            introManager.IntroAudioSource.Play();
        }
    }
}