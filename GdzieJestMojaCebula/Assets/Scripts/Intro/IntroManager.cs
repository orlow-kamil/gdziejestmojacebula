﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Intro
{
    [RequireComponent(typeof(AudioSource))]
    public class IntroManager : MonoBehaviour
    {
        [SerializeField] private Camera myRoomCamera = null;
        [SerializeField] private Camera kitchenCamera = null;

        public AudioSource IntroAudioSource { get; set; }

        private void Awake() => IntroAudioSource = GetComponent<AudioSource>();
        private void OnEnable() => ChangeRoom(false);


        public void ChangeRoom(bool goToKitchen)
        {
            if (!goToKitchen)
            {
                myRoomCamera.gameObject.SetActive(true);
                kitchenCamera.gameObject.SetActive(false);
            }
            else
            {
                myRoomCamera.gameObject.SetActive(false);
                kitchenCamera.gameObject.SetActive(true);
            }
        }

        public void NextScene() => SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings + 1);
    }
}