﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.Start
{
    public class MainMenu : MonoBehaviour
    {
        public void PlayGame()
        {
            Debug.Log("rozpoczynam gre");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        public void Credits()
        {
            Debug.Log("otwieram creditsy");
            SceneManager.LoadScene("Credits");
        }
        public void Exit()
        {
            Debug.Log("Wychodze z gry");
            Application.Quit();
        }

    }
}