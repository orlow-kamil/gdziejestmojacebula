﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.Start
{
    public class CreditsMenu : MonoBehaviour
    {
        public void MainMenu()
        {
            SceneManager.LoadScene("Menu");
        }
    }
}