﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.Gameplay
{
    public class EquipmentSlot : MonoBehaviour
    {
        [SerializeField][Range(0,2)] private int index = 0;
        [SerializeField] private Sprite startSprite = null;

        public int Index => index;
        public Sprite StartSprite => startSprite;

        public Sprite GetEquipmentSlotSprite() => this.GetComponentInChildren<Image>().sprite;
        public void SetEquipmentSlotSprite(Sprite sprite) => this.GetComponentInChildren<Image>().sprite = sprite;
        public void ResetEquipmentSlotSpriteToDefault() => SetEquipmentSlotSprite(startSprite);
    }
}
