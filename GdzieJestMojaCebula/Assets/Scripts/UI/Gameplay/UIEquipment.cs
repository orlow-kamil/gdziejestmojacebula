﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace UI.Gameplay
{
    public class UIEquipment : MonoBehaviour
    {
        public List<EquipmentSlot> EquipmentSlotList { get; private set; }

        private void OnEnable()
        {
            this.EquipmentSlotList = this.GetComponentsInChildren<EquipmentSlot>().ToList<EquipmentSlot>();
            this.EquipmentSlotList.OrderBy(x => x.GetComponentInChildren<EquipmentSlot>().Index);
        }

        public void SetupWeaponToEquipmentSlot(Sprite sprite, int index) => this.EquipmentSlotList.ElementAtOrDefault(index).SetEquipmentSlotSprite(sprite);
        public void RemoveWeaponFromEquipmentSlot(int index) => this.EquipmentSlotList.ElementAtOrDefault(index).ResetEquipmentSlotSpriteToDefault();
        public int GetAvaviableIndexFromEquipmentSlotList() => this.EquipmentSlotList.FirstOrDefault(x => x.GetEquipmentSlotSprite() == x.StartSprite).Index;
        public int GetIndexFromEquipmentSlotList(Sprite sprite) => this.EquipmentSlotList.FirstOrDefault(x => x.GetEquipmentSlotSprite() == sprite).Index;
    }
}